<div class="container">

	<div class="row">

		<div class="col-lg-12 col-md-12">
			<h2 class="pagename-custom">Cantonment Executive Officer (CEO)</h2>
			<hr>
		</div>

	</div>

<div class="well">
<h3 class="membername-custom">Message From CEO</h3>
<p>
Welcome to the Website of Cantonment Board Chaklala. This site is designed to provide accurate and easy to use information on Cantonment Board services and programs.
Cantonment Board Chaklala has been playing very active role in overall development of the Cantonment. Over the years the state of affairs of roads, lands and parks has been revamped and the people being facilitated through provision of water supply and primary health, Besides improving on sanitary condition and infrastructure development.
Through enough has been done but there is long way to go. We take pride in being sensitive to the needs of the cantonment residents that we serve. It is our endeavor to improve the standard of living of its residents by continuous improvements in civic amenities and by executing new developmental projects. It will be appropriated if we ensure that the places where we work, where we reside, are always clean. Cantonment Board Chaklala has a vision to improve it further by providing a healthy environment.
Our efforts are geared towards public awareness and to this end Cantonment Board has launched this website with a view to enlist the support /feedback and cooperation of people of the Cantonment and also to provide the information through easier mode. This website is an effort to bring forth the facts, profile and functions of the Cantonment Board. Besides it is a step forward to ensure transparency in the functioning of the organization and to reflect its achievements, progress and vision. I am optimistic that this website will benefit the public at large in general and the Taxpayers of Chaklala Cantonment in particular. The suggestions of the public shall be inputs for making Cantonment Board Chaklala, a model Cantonment.
<br><br>Regards,<br>
Asif Ameer Khan
</p>
</div>

</div>