<div class="container"> 
    <div class="row"> 
        <div class="col-lg-12 col-md-12"> 
            <h2 class="pagename-custom">Water Supply Network</h2> 
            <hr> 
        </div>         
    </div>     
</div>
<section class="content-block content-1-8">
    <div class="container">
        <ul class="nav text-center nav-justified nav-tabs" role="tablist" id="myTab">
            <li class="active">
                <a href="#tab1" role="tab" data-toggle="tab">Water Supply</a>
            </li>
            <li>
                <a href="#tab2" role="tab" data-toggle="tab">Tube Wells</a>
            </li>
            <li>
                <a href="#tab3" role="tab" data-toggle="tab">Filtration Plants</a>
            </li>
            <li>
                <a href="#tab4" role="tab" data-toggle="tab">Water Tanks</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active" id="tab1">
                <div class="row">
                    <div class="col-md-5 col-sm-6">
                        <p>Chaklala Cantonment Board maintains a wide network of water supply proving water for the residents of Chaklala Cantonment.</p>
                        <p>CCB have maintained 43 Tube wells, 17 underground water tank and 11 overhead water tanks are in Chaklala Cantt area. There are 17 water filtration plants installed in Chaklala Cantt for provision of pure and hygienic water to the residents of Cantt area. All filtration plants are functional and well maintained. The cartridges are being changed on regular basis after every 03 months. This office has facilitated the following area of Chaklala Cantt: -</p>
                        <ul> 
                            <li>Askari-I, II, III, IV, V, VII, VIII, IX</li>
                            <li>Chaklala Scheme-I</li>
                            <li>Chaklala Scheme-II</li>
                            <li>Chaklala Scheme-III</li>
                            <li>Dheri Hassanabad</li>
                            <li>Dhoke Chiraghdin</li>
                            <li>Dhoke Piran Faqiran</li>
                            <li>Ghazi Colony</li>
                            <li>Jahangir Road</li>
                            <li>Jhanda Chichi</li>
                            <li>Jhawara</li>
                            <li>Lalazar</li>
                            <li>Marrir Hassan</li>
                            <li>Muslimabad</li>
                            <li>Nadeemabad</li>
                            <li>Rahimabad</li>
                            <li>Sir Syed Colony</li>
                            <li>Sir Syed Road</li>
                            <li>Tahli Mohri</li>
                            <li>Tehmaspabad</li>
                            <li>Tulsa</li>
                            <li>Walayat Colony</li>
                        </ul>
                        <br>
                    </div>
                    <div class="col-sm-6 col-md-offset-1 col-md-5">
                        <h3 class="membername-custom">Sources of Water</h3>
                        <table class="table table-striped text-left table-bordered">
                            <tbody>
                                <tr>
                                    <td>Tubewells</td>
                                    <td>1.674 MG/Daily</td>
                                </tr>
                                <tr>
                                    <td>Rawal Dam Source through MES</td>
                                    <td>1.000 MG/Daily</td>
                                </tr>
                                <tr>
                                    <td>Khanpur Dam	Source through RCB</td>
                                    <td>0.400 MG/Daily</td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                        <h3 class="membername-custom">Daily Demand vs Daily Supply</h3>
                        <table class="table table-striped text-left table-bordered">
                            <tbody>
                                <tr>
                                    <td>Current Population (Approx)</td>
                                    <td>0.355 M (persons</td>
                                </tr>
                                <tr>
                                    <td>Water supply served through all sources</td>
                                    <td>80 % of population</td>
                                </tr>
                                <tr>
                                    <td>Water demand</td>
                                    <td>30 Gallons per day per capita</td>
                                </tr>
                                <tr>
                                    <td>Total daily demand</td>
                                    <td>8.520 MGD</td>
                                </tr>
                                <tr>
                                    <td>Daily Average Supply</td>
                                    <td>3.074 MGD</td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                        <h3 class="membername-custom">Contact Persons - Water Supply Network</h3>
                        <table class="table table-striped text-left table-bordered">
                            <tbody>
                                <tr>
                                    <td>Mr Fazail, Assistant Cantonment Engineer (ACE)</td>
                                    <td>0333-5119005</td>
                                </tr>
                                <tr>
                                    <td>Mr Arshad Bhatti, Cantonment Overseer (CO)</td>
                                    <td>0323-3572952</td>
                                </tr>
                                <tr>
                                    <td>Mr Muhammad Ramzan Chargehand (Zone-1(A))</td>
                                    <td>0333-5635028</td>
                                </tr>
                                <tr>
                                    <td>Mr Muhammad Riaz Chargehand (Zone-1(B))</td>
                                    <td>0321-5624610</td>
                                </tr>
                                <tr>
                                    <td>Mr Akbar Ali Chargehand (Zone-2)</td>
                                    <td>0345-5400271</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.col -->
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <div class="tab-pane fade" id="tab2">
                <!-- /.row -->
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 text-center">
                        <h3 class="membername-custom">List of Tube Wells</h3>
                        <table class="table table-striped table-bordered">
                            <tbody>
                                <thead class="">
                                    <tr class="">
                                        <th class="text-center">S.No</th>
                                        <th class="text-center">Location</th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td>1</td>
                                    <td>Alaf Shah Graveyard under road</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Askari-III, Chaklala-III</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Askari-IV at park tube well</td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Askari-IV near Block No.33</td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>Askari-IV, main road Jhanda Chichi</td>
                                </tr>
                                <tr>
                                    <td>6</td>
                                    <td>Askari-IX near Airport</td>
                                    
                                </tr>
                                <tr>
                                    <td>7</td>
                                    <td>Askari-VII Adiala Road - 1</td>
                                </tr>
                                <tr>
                                    <td>8</td>
                                    <td>Askari-VII Adiala Road - 2</td>
                                </tr>
                                <tr>
                                    <td>9</td>
                                    <td>Askari-VIII near Airport.</td>
                                </tr>
                                <tr>
                                    <td>10</td>
                                    <td>At Nursery Park (newly installed grant-in-aid Mr.Hanif Abbasi)</td>
                                </tr>
                                <tr>
                                    <td>11</td>
                                    <td>At Rose Garden Park (newly installed grant-in-aid Mr.Hanif Abbasi</td>
                                </tr>
                                <tr>
                                    <td>12</td>
                                    <td>At Sarwar Road near Ziarat</td>
                                </tr>
                                <tr>
                                    <td>13</td>
                                    <td>At Sir Syed Road</td>
                                </tr>
                                <tr>
                                    <td>14</td>
                                    <td>At Water Works Chaklala – III</td>
                                </tr>
                                <tr>
                                    <td>15</td>
                                    <td>Dhoke Chiragh Din near Girls School</td>
                                </tr>
                                <tr>
                                    <td>16</td>
                                    <td>Dhoke Chiraghdin near Imam Bargh</td>
                                </tr>
                                <tr>
                                    <td>17</td>
                                    <td>Dhoke Chiraghdin near Imam Bargh street No.10 near filter plant</td>
                                </tr>
                                <tr>
                                    <td>18</td>
                                    <td>Dhoke Kashmirian</td>
                                </tr>
                                <tr>
                                    <td>19</td>
                                    <td>Ghazi Colony (new)</td>
                                </tr>
                                <tr>
                                    <td>20</td>
                                    <td>Ghazi Colony Chaklala-II</td>
                                </tr>
                                <tr>
                                    <td>21</td>
                                    <td>Jhanda Chi Chi</td>
                                </tr>
                                <tr>
                                    <td>22</td>
                                    <td>Jhanda Chichi Magistrate Colony</td>
                                </tr>
                                <tr>
                                    <td>23</td>
                                    <td>Kousar Market, Dhoke Chiraghdin</td>
                                    
                                </tr>
                                <tr>
                                    <td>24</td>
                                    <td>Maqboolabad</td>
                                </tr>
                                <tr>
                                    <td>25</td>
                                    <td>Marrir Hassan  near Railway Line</td>
                                </tr>
                                <tr>
                                    <td>26</td>
                                    <td>Marrir Hassan near C.B Quarters</td>
                                </tr>
                                <tr>
                                    <td>27</td>
                                    <td>Muslimabad</td>
                                </tr>
                                <tr>
                                    <td>28</td>
                                    <td>National Park Road Officer Colony</td>
                                </tr>
                                <tr>
                                    <td>29</td>
                                    <td>Near Viqar-un-Nisa near Noashi Masjid</td>
                                </tr>
                                <tr>
                                    <td>30</td>
                                    <td>Rahimabad</td>
                                </tr>
                                <tr>
                                    <td>31</td>
                                    <td>Rahimabad (new)</td>
                                </tr>
                                <tr>
                                    <td>32</td>
                                    <td>Rose Garden Park Ckl-III</td>
                                </tr>
                                <tr>
                                    <td>33</td>
                                    <td>Stree No.8 Chaklala Scheme-III. (newly installed grant-in-aid Mr.Hanif Abbasi</td>
                                </tr>
                                <tr>
                                    <td>34</td>
                                    <td>Street No.10 Qaziabad</td>
                                </tr>
                                <tr>
                                    <td>35</td>
                                    <td>Street No.8, Chaklala Scheme-III</td>
                                </tr>
                                <tr>
                                    <td>36</td>
                                    <td>Tehmaspabad (new)</td>
                                </tr>
                                <tr>
                                    <td>37</td>
                                    <td>Tehmaspabad (old)</td>
                                    
                                </tr>
                                <tr>
                                    <td>38</td>
                                    <td>Triangle Park Ckl-II</td>
                                </tr>
                                <tr>
                                    <td>39</td>
                                    <td>Tube well  near P.C Hotel</td>
                                </tr>
                                <tr>
                                    <td>40</td>
                                    <td>Tube well at Dheri Hassanabad  Dheri Dispensary</td>
                                </tr>
                                <tr>
                                    <td>41</td>
                                    <td>Tube well at Dheri Hassanabad near filter plant</td>
                                </tr>
                                <tr>
                                    <td>42</td>
                                    <td>Tube well at Lane No.3, Burhan-ud-Din Lalazar</td>
                                </tr>
                                <tr>
                                    <td>43</td>
                                    <td>Tube well at Nursery Park Ckl-III</td>
                                </tr>
                                <tr>
                                    <td>44</td>
                                    <td>Tube well at Presidency</td>
                                </tr>
                                <tr>
                                    <td>45</td>
                                    <td>Tube well at Railway Scheme-4 (pipe line street)</td>
                                </tr>
                                <tr>
                                    <td>46</td>
                                    <td>Tube well Fazal-e-Haq Road Lane No.4 Lalazar</td>
                                </tr>
                                <tr>
                                    <td>47</td>
                                    <td>Tube well Jhanda Chichi Barki Bagh</td>
                                </tr>
                                <tr>
                                    <td>48</td>
                                    <td>Tube well Jhanda Chichi Barki Bagh near Railway Line</td>
                                </tr>
                                <tr>
                                    <td>49</td>
                                    <td>Tube well near F.G Girls School Dheri Hassanabad near Alaf Shah Graveyard</td>
                                </tr>
                                <tr>
                                    <td>50</td>
                                    <td>Tube well near Hockey Stadium</td>
                                </tr>
                                <tr>
                                    <td>51</td>
                                    <td>Tube well near OHT Jhanda Chichi</td>
                                </tr>
                                <tr>
                                    <td>52</td>
                                    <td>Tube well near UGT Lalkurti</td>
                                </tr>
                                <tr>
                                    <td>53</td>
                                    <td>Tube well street No.12, Chaklala Scheme-III</td>
                                </tr>
                                <tr>
                                    <td>54</td>
                                    <td>Viqar-un-Nisa College Jahangir</td>
                                </tr>
                                <tr>
                                    <td>55</td>
                                    <td>Water works Tipu Road</td>
                                </tr>
                                <tr>
                                    <td>56</td>
                                    <td>Water Works Tipu Road</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /#tab1 -->
            <!-- /#tab2 -->
            <div class="tab-pane fade" id="tab3">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 text-center col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">
                        <h3 class="membername-custom">List of Filtration Plants</h3>
                        <table class="table table-striped table-bordered">
                            <tbody>
                                <thead class="">
                                    <tr class="">
                                        <th class="text-center">S.No</th>
                                        <th class="text-center">Location</th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td>1</td>
                                    <td>Askari-I</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Askari-6</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Chaklala Scheme-III, Water Works</td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Dheri Hassanabad Alaf Shah Graveyard</td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>Dheri Hassanabad Dispensary</td>
                                </tr>
                                <tr>
                                    <td>6</td>
                                    <td>Dhoke Chiraghdin near Overhead Water Tank</td>
                                    
                                </tr>
                                <tr>
                                    <td>7</td>
                                    <td>Dhoke Chiraghdin near tube well No.31</td>
                                </tr>
                                <tr>
                                    <td>8</td>
                                    <td>Dhoke Kashmirian</td>
                                </tr>
                                <tr>
                                    <td>9</td>
                                    <td>Jhanda Chichi</td>
                                </tr>
                                <tr>
                                    <td>10</td>
                                    <td>Jhangir Road near Viqar-un-Nisa School</td>
                                </tr>
                                <tr>
                                    <td>11</td>
                                    <td>Marrir Hassan tube well No.34</td>
                                </tr>
                                <tr>
                                    <td>12</td>
                                    <td>National Park Road</td>
                                </tr>
                                <tr>
                                    <td>13</td>
                                    <td>Near tube well No.22, Tehmaspabad</td>
                                </tr>
                                <tr>
                                    <td>14</td>
                                    <td>Near tube well No.24, Rahimabad</td>
                                </tr>
                                <tr>
                                    <td>15</td>
                                    <td>Tariqabad near Lalkurti</td>
                                </tr>
                                <tr>
                                    <td>16</td>
                                    <td>Tulsa Village Lalazar</td>
                                </tr>
                                <tr>
                                    <td>17</td>
                                    <td>Water works Tipu Road</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <div class="tab-pane fade" id="tab4">
                <!-- /.row -->
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-5 col-lg-5 text-center">
                        <h3 class="membername-custom">List of Underground Water Tanks</h3>
                        <table class="table table-striped table-bordered">
                            <tbody>
                                <thead class="">
                                    <tr class="">
                                        <th class="text-center">S.No</th>
                                        <th class="text-center">Location</th>
                                        <th class="text-center">Capacity</th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td>1</td>
                                    <td>Askari flats Sabzazar</td>
                                    <td>20,000</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Askari flats Willoughby Road</td>
                                    <td>50,000</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Askari-IV opp. Jhanda Chichi</td>
                                    <td>30,000</td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Askari-VII Adiala Road</td>
                                    <td>100,000</td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>Chaklala Scheme-I</td>
                                    <td>100,000</td>
                                </tr>
                                <tr>
                                    <td>6</td>
                                    <td>Dheri Dispensary</td>
                                    <td>15,000</td>
                                </tr>
                                <tr>
                                    <td>7</td>
                                    <td>National Park Road</td>
                                    <td>100,000</td>
                                </tr>
                                <tr>
                                    <td>8</td>
                                    <td>Near 502 Workshop Adyala Road</td>
                                    <td>100,000</td>
                                </tr>
                                <tr>
                                    <td>9</td>
                                    <td>Near Alaf Shah Graveyard Dheri Hassanabad</td>
                                    <td>200,000</td>
                                </tr>
                                <tr>
                                    <td>10</td>
                                    <td>Water works Askari-I</td>
                                    <td>100,000</td>
                                </tr>
                                <tr>
                                    <td>11</td>
                                    <td>Water works Askari-II</td>
                                    <td>100,000</td>
                                </tr>
                                <tr>
                                    <td>12</td>
                                    <td>Water works Askari-III</td>
                                    <td>100,000</td>
                                </tr>
                                <tr>
                                    <td>13</td>
                                    <td>Water works Chaklala-I</td>
                                    <td>25,000</td>
                                </tr>
                                <tr>
                                    <td>14</td>
                                    <td>Water works Chaklala-III</td>
                                    <td>100,000</td>
                                </tr>
                                <tr>
                                    <td>15</td>
                                    <td>Water works Lalazar</td>
                                    <td>100,000</td>
                                </tr>
                                <tr>
                                    <td>16</td>
                                    <td>Water works Tariqabad</td>
                                    <td>50,000</td>
                                </tr>
                                <tr>
                                    <td>17</td>
                                    <td>Water works Tipu Road</td>
                                    <td>100,000</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-12 col-xs-12 col-md-5 col-lg-5 col-md-offset-1 col-lg-offset-1 text-center">
                        <h3 class="membername-custom">List of Overhead Water Tanks</h3>
                        <table class="table table-striped table-bordered">
                            <tbody>
                                <thead class="">
                                    <tr class="">
                                        <th class="text-center">S.No</th>
                                        <th class="text-center">Location</th>
                                        <th class="text-center">Capacity</th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td>1</td>
                                    <td>Askari-IV opp. Jhanda Chichi</td>
                                    <td>25,000</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Askari-VII Adiala Road - 1</td>
                                    <td>50,000</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Askari-VII Adiala Road - 2</td>
                                    <td>30,000</td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Askari-VIII &amp; IX near Airport.</td>
                                    <td>50,000</td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>Dheri Hassanabad</td>
                                    <td>200,000</td>
                                </tr>
                                <tr>
                                    <td>6</td>
                                    <td>Golf Road</td>
                                    <td>50,000</td>
                                </tr>
                                <tr>
                                    <td>7</td>
                                    <td>National Park Road</td>
                                    <td>50,000</td>
                                </tr>
                                <tr>
                                    <td>8</td>
                                    <td>Water works Askari-I.</td>
                                    <td>50,000</td>
                                </tr>
                                <tr>
                                    <td>9</td>
                                    <td>Water works Chaklala-I</td>
                                    <td>100,000</td>
                                </tr>
                                <tr>
                                    <td>10</td>
                                    <td>Water works Chaklala-III</td>
                                    <td>100,000</td>
                                </tr>
                                <tr>
                                    <td>11</td>
                                    <td>Water works Lalazar</td>
                                    <td>100,000</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /#tab3 -->
            <!-- /#tab4 -->
        </div>
        <!-- /.tab-content -->
    </div>
    <!-- /.container -->
</section>