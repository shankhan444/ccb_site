<div class="container">

	<div class="row">

		<div class="col-lg-12 col-md-12">
			<h2 class="pagename-custom">CB Schools and Colleges</h2>
			<hr>
		</div>

	</div>

		<!-- Carousel Start -->
		<div id="myCarousel" class="carousel slide" data-ride="carousel" style="margin-bottom: 20px;">
    <!-- Carousel indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
        <li data-target="#myCarousel" data-slide-to="4"></li>
    </ol>   
    <!-- Wrapper for carousel items -->
    <div class="carousel-inner">
        <div class="item active">
            <img src="<?php echo $base_url ?>images/education/school/slider/1.jpg" alt="First Slide">
        </div>
        <div class="item">
            <img src="<?php echo $base_url ?>images/education/school/slider/2.jpg" alt="Second Slide">
        </div>
        <div class="item">
            <img src="<?php echo $base_url ?>images/education/school/slider/3.jpg" alt="Third Slide">
        </div>
        <div class="item">
            <img src="<?php echo $base_url ?>images/education/school/slider/4.jpg" alt="Third Slide">
        </div>
        <div class="item">
            <img src="<?php echo $base_url ?>images/education/school/slider/5.jpg" alt="Third Slide">
        </div>
    </div>
    <!-- Carousel controls -->
    <a class="carousel-control left" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="carousel-control right" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
</div>
		<!-- Carousel End -->

    <div class="row">

		<div class="col-lg-12 col-md-12">
			<p>Chaklala Cantonment Board is providing education facilities to its residents through one College, one High School and four CB Model Schools.</p>
            <p>CB Schools have qualified and trained school faculty.</p>
            <table class="table table-striped table-bordered text-center">
                            <tbody>
                                <thead class="">
                                    <tr class="">
                                        <th class="text-center">S.No</th>
                                        <th class="text-center">Name of School</th>
                                        <th class="text-center">No. of Teachers</th>
                                        <th class="text-center">No. of Students</th>
                                        <th class="text-center">Fee Structure per student</th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td>1</td>
                                    <td>Cantt Montessori &amp; Junior School, Sabzazar</td>
                                    <td>17</td>
                                    <td>207</td>
                                    <td>Rs. 750</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Cb Model School, Tulsa Lalazar</td>
                                    <td>09</td>
                                    <td>142</td>
                                    <td>Rs. 30</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Cb Model School, Dhoke Chiragh Din</td>
                                    <td>08</td>
                                    <td>160</td>
                                    <td>Rs. 30</td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Cb Model School, Jhanda Chichi</td>
                                    <td>07</td>
                                    <td>127</td>
                                    <td>Rs. 30</td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>Cb Model School, Sir Syed Colony</td>
                                    <td>09</td>
                                    <td>111</td>
                                    <td>Rs. 30</td>
                                </tr>
                                <tr>
                                    <td>6</td>
                                    <td>Cb Model School, Tahli Mohri</td>
                                    <td>07</td>
                                    <td>103</td>
                                    <td>Rs. 30</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Total</td>
                                    <td>57</td>
                                    <td>872</td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
		</div>

	</div>
    <br>
    
</div>