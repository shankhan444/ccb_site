<div class="container">

	<div class="row">

		<div class="col-lg-12 col-md-12">
			<h2 class="pagename-custom">Water Supply Network</h2>
			<hr>
		</div>

	</div>

<div class="well">
<p>
<ul class="list-style-disc">
<li>
Chaklala Cantonment Board maintains a wide network of water supply proving water for the residents of Chaklala Cantonment.
</li>
<li>
CCB have maintained 43 Tube wells, 17 underground water tank and 11 overhead water tanks are in Chaklala Cantt area. There are 17 water filtration plants installed in Chaklala Cantt for provision of pure and hygienic water to the residents of Cantt area. All filtration plants are functional and well maintained. The cartridges are being changed on regular basis after every 03 months. This office has facilitated the following area of Chaklala Cantt: -
</li>
	<ul class="list-style-circle">
		<li>Askari-I, II, III, IV, V, VII, VIII, IX</li>
		<li>Chaklala Scheme-I</li>
		<li>Chaklala Scheme-II</li>
		<li>Chaklala Scheme-III</li>
		<li>Dheri Hassanabad</li>
		<li>Dhoke Chiraghdin</li>
		<li>Dhoke Piran Faqiran</li>
		<li>Ghazi Colony</li>
		<li>Jahangir Road</li>
		<li>Jhanda Chichi</li>
		<li>Jhawara</li>
		<li>Lalazar</li>
		<li>Marrir Hassan</li>
		<li>Muslimabad</li>
		<li>Nadeemabad</li>
		<li>Rahimabad</li>
		<li>Sir Syed Colony</li>
		<li>Sir Syed Road</li>
		<li>Tahli Mohri</li>
		<li>Tehmaspabad</li>
		<li>Tulsa</li>
		<li>Walayat Colony</li>
	</ul>
</ul>
</p>
<p>
<ul>
<li>Sources of Water</li>
<div class="row">
	<ul>
		<li>
			<div class="col-lg-3 col-md-6">
				Tubewells 
			</div>
			<div class="col-lg-3 col-md-6">
				1.674 MG/Daily
			</div>
		</li>
		<li>
			<div class="col-lg-3 col-md-6">
				Rawal Dam Source through MES 
			</div>
			<div class="col-lg-3 col-md-6">
				1.000 MG/Daily
			</div>
		</li>
		<li>
			<div class="col-lg-3 col-md-6">
				Khanpur Dam	Source through RCB 
			</div>
			<div class="col-lg-3 col-md-6">
				0.400 MG/Daily
			</div>
		</li>
	</ul>
</div>
</ul>
</p>

<p>
	<ul>
		<li>Daily Demand vs Daily Supply</li>
		<div class="row">
			<ul>
				<li>
        			<div class="col-lg-4 col-md-6">
        				Current Population (Approx) 
        			</div>
        			<div class="col-lg-4 col-md-6">
        				0.355 M (persons)
        			</div>
        		</li>
        		<li>
        			<div class="col-lg-4 col-md-6">
        				Water supply served through all sources
        			</div>
        			<div class="col-lg-4 col-md-6">
        				80 % of population
        			</div>
        		</li>
        		<li>
        			<div class="col-lg-4 col-md-6">
        				Water demand
        			</div>
        			<div class="col-lg-4 col-md-6">
        				30 Gallons per day per capita
        			</div>
				</li>
				<li>
        			<div class="col-lg-4 col-md-6">
        				Total daily demand
        			</div>
        			<div class="col-lg-4 col-md-6">
        				8.520 MGD
        			</div>
				</li>
				<li>
        			<div class="col-lg-4 col-md-6">
        				Daily Average Supply
        			</div>
        			<div class="col-lg-4 col-md-6">
        				3.074 MGD
        			</div>
				</li>
				<li>
        			<div class="col-lg-4 col-md-6">
        				Shortfall
        			</div>
        			<div class="col-lg-4 col-md-6">
        				5.446 MGD
        			</div>
				</li>
			</ul>
		</div>
	</ul>
</p>
</div>

</div>