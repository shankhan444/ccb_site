<div class="container">

	<div class="row">

		<div class="col-lg-12 col-md-12">
			<h2 class="pagename-custom">President</h2>
			<hr>
		</div>

	</div>

<div class="well">
<h3 class="membername-custom">Message From President</h3>
<p>
It is Indeed a source of grant satisfaction for me to dwell on the achievements of CCB for improving on the existing facilities besides commendable efforts for infrastructure development i.e widening of existing roads to accommodate growing traffic, improving water supply and drain lines and providing street lighting in every corner of the Cantonment.
The Board Has taken up several ventures that have made a commendable difference to the quality of the residents. Due to efforts made by the Board, the Cantonment now boasts of clean, tree lined roads, decent water supply position, good street lightening and hygienic environment.
But despite the best efforts of CCB, the general public has to shoulder their responsibilities. They have to make proper use of the facilities provided by the board. It is expected that the residents put the domestic waste in the dust bins provided for that purpose. Taxes and other charges should be paid in time. Activities like unauthorized constructions and encroachments on government land should be avoided. It is only with the active participation from residents that any initiative taken up by the board can be a success.
The Cantonment Board Chaklala has launched this website to provide awareness and knowledge to its inhabitants at large. The website has been launched with an aim to facilitate general public in day to day dealings with Cantonment Board Chaklala. This portal is an attempt by the Cantonment Board to make comprehensible the complex terminology, rules and procedures to the residents of the cantonment so as to make them active participants in the process of governance in place of passive receivers of information from the government.
I commend the efforts put in by the CEO and the staff in making this project a reality in such a short time using available resources.<br><br>
President CCB,<br>
Brig. Saeed Raza Hassan
</p>
</div>

</div>