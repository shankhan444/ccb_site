<div class="container">

	<div class="row">

		<div class="col-lg-12 col-md-12">
			<h2 class="pagename-custom">Civil Members</h2>
			<hr>
		</div>

	</div>

<div class="well jumbo-well-custom">
<p>
Cantonment Local Government elections were held in 2015. Chaklala Cantonment Board being class I cantonment has been divided into 10 wards. Each ward has one elected member. There are two special seats. Hence total civil members are 12. 
</p>
</div>
<!-- First row start -->
<div class="row">
<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 centerimage-custom">
<img alt="" src="<?php echo $base_url; ?>images/civil_members/1.jpg">
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
<h3 class="membername-custom">Raja Irfan Imtiaz</h3>
<h4 class="text-primary">Vice President</h4>
<p>
<strong>Ward No.</strong> 9 <br>
<strong>Area:</strong> Tahli Mohri, Jhawra, Riaz Qureshi Road, Aslam Shaheed Road, Dhamial Road <br>
<strong>Party Affiliation:</strong> PML (N) <br>
</p>
</div>

<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 centerimage-custom">
<img alt="" src="<?php echo $base_url; ?>images/civil_members/2.jpg">
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
<h3 class="membername-custom">Raja Parvaiz Akhtar</h3>
<p>
<strong>Ward No.</strong> 1 <br>
<strong>Area:</strong> Wavel Line, Gracey Line, Chaklala Railway Station, Airport Road, Rawal Road, Railway Scheme-I <br>
<strong>Party Affiliation:</strong> PML (N) <br>
</p>
</div>

</div>
<!-- First row end -->
<hr class="custom-hr">
<!-- Second row start -->
<div class="row">
<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 centerimage-custom">
<img alt="" src="<?php echo $base_url; ?>images/civil_members/11.jpg">
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
<h3 class="membername-custom">Ch. Iftikhar Ahmed</h3>
<h4 class="text-primary">Special interest seats Peasant member</h4>
<p>
<strong>Ward No.</strong> 8 <br>
<strong>Party Affiliation:</strong> PML (N) <br>
</p>
</div>

<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 centerimage-custom">
<img alt="" src="<?php echo $base_url; ?>images/civil_members/12.jpg">
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
<h3 class="membername-custom">Mr. Pervaiz Aziz</h3>
<h4 class="text-primary">Special interest seats Minority member</h4>
<p>
<strong>Ward No.</strong> 10 <br>
<strong>Party Affiliation:</strong> PML (N) <br>
</p>
</div>

</div>
<!-- Second Row End -->
<hr class="custom-hr">
<!-- Third Row Start -->
<div class="row">
<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 centerimage-custom">
<img alt="" src="<?php echo $base_url; ?>images/civil_members/5.jpg">
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
<h3 class="membername-custom">Mian Muhammad Riaz</h3>
<p>
<strong>Ward No.</strong> 4 <br>
<strong>Area:</strong> Chaklala Scheme-I, III, Garrison HQs, Askari-I, II, III, V, Walayat Colony <br>
<strong>Party Affiliation:</strong> PML (N) <br>
</p>
</div>

<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 centerimage-custom">
<img alt="" src="<?php echo $base_url; ?>images/civil_members/6.jpg">
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
<h3 class="membername-custom">Mr. Khalid Mehmood Butt</h3>
<p>
<strong>Ward No.</strong> 5 <br>
<strong>Area:</strong> Gulistan Colony, Morgah, G.T Road, Alshifa Eye Trust, Lane No.2 Lalazar <br>
<strong>Party Affiliation:</strong> PML (N) <br>
</p>
</div>

</div>
<!-- Third Row End -->
<hr class="custom-hr">
<!-- Fourth row start -->
<div class="row">
<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 centerimage-custom">
<img alt="" src="<?php echo $base_url; ?>images/civil_members/7.jpg">
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
<h3 class="membername-custom">Mr. Khurram Siddique</h3>
<p>
<strong>Ward No.</strong> 6 <br>
<strong>Area:</strong> Marrir Hassan new Abadi, Sir Syed Road, Mall Road, Shaheed Colony Sabzazar <br>
<strong>Party Affiliation:</strong> PML (N) <br>
</p>
</div>

<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 centerimage-custom">
<img alt="" src="<?php echo $base_url; ?>images/civil_members/8.jpg">
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
<h3 class="membername-custom">Mr. Muhammad Jamil</h3>
<p>
<strong>Ward No.</strong> 7 <br>
<strong>Area:</strong> Tariqabad, Lalkurti, Indus Road, OP No.22 Harley Street, Riazabad <br>
<strong>Party Affiliation:</strong> PML (N) <br>
</p>
</div>

</div>
<!-- Fourth row end -->
<hr class="custom-hr">
<!-- Fifth row start -->
<div class="row">
<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 centerimage-custom">
<img alt="" src="<?php echo $base_url; ?>images/civil_members/9.jpg">
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
<h3 class="membername-custom">Mr. Khurram Shahzad</h3>
<p>
<strong>Ward No.</strong> 8 <br>
<strong>Area:</strong> Dheri Hassanabad, New Harley Street, Abdul Ghani Road, Dhoke Shera Lalazar <br>
<strong>Party Affiliation:</strong> PML (N) <br>
</p>
</div>

<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 centerimage-custom">
<img alt="" src="<?php echo $base_url; ?>images/civil_members/10.jpg">
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
<h3 class="membername-custom">Malik Azhar Naeem</h3>
<p>
<strong>Ward No.</strong> 10 <br>
<strong>Area:</strong> Askari-XIII, VII, Zafar Akbar Road, Mazhar Qayyum Road, Tulsa Road (southern side), Sherzaman Colony <br>
<strong>Party Affiliation:</strong>  <br>
</p>
</div>

</div>
<!-- Fifth row end -->
<hr class="custom-hr">
<!-- Sixth row start -->
<div class="row">
<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 centerimage-custom">
<img alt="" src="<?php echo $base_url; ?>images/civil_members/3.jpg">
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
<h3 class="membername-custom">Mirza Khalid Mehmood</h3>
<p>
<strong>Ward No.</strong> 2 <br>
<strong>Area:</strong> Sir Syed Colony, Rahimabad, Chaklala Scheme-II Dhoke Chiraghdin, Ghazi Colony, Railway Scheme-II & III <br>
<strong>Party Affiliation:</strong> Jamaat-e-Islami <br>
</p>
</div>

<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 centerimage-custom">
<img alt="" src="<?php echo $base_url; ?>images/civil_members/4.jpg">
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
<h3 class="membername-custom">Ch. Changez Khan</h3>
<p>
<strong>Ward No.</strong> 3 <br>
<strong>Area:</strong> Jhanda Chichi, shami Road Askari-10 & 4 <br>
<strong>Party Affiliation:</strong> PML (N) <br>
</p>
</div>

</div>
<!-- Sixth row end -->
</div>
<hr>