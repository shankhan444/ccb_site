<div class="container"> 
    <div class="row"> 
        <div class="col-lg-12 col-md-12"> 
            <h2 class="pagename-custom">Water Supply Network</h2> 
            <hr> 
        </div>         
    </div>          
</div>
<section class="content-block content-1-8">
    <div class="container">
        <ul class="nav nav-tabs text-center" role="tablist" id="myTab">
            <li class="active">
                <a href="#tab1" role="tab" data-toggle="tab">Design</a>
            </li>
            <li>
                <a href="#tab2" role="tab" data-toggle="tab">Development</a>
            </li>
            <li>
                <a href="#tab3" role="tab" data-toggle="tab">Bootstrap</a>
            </li>
            <li>
                <a href="#tab4" role="tab" data-toggle="tab">WordPress</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active" id="tab1">
                <div class="row">
                    <div class="col-md-5 col-md-offset-1 col-sm-6">
                        <img class="img-responsive" src="http://placehold.it/600x400">
                    </div>
                    <!-- /.col -->
                    <div class="col-md-5 col-sm-6">
                        <h3>Design</h3>
                        <p>Magnis modipsae que lib voloratati andigen daepedor quiate ut reporemni aut labor. Laceaque quiae sitiorem ut restibusaes es tumquam core posae volor remped modis volor. Doloreiur qui commolu ptatemp dolupta orem retibusam emnis et consent accullignis lomnus.</p>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /#tab1 -->
            <div class="tab-pane fade" id="tab2">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 text-center">
                        <h3>Development</h3>
                        <p>Magnis modipsae que lib voloratati andigen daepedor quiate ut reporemni aut labor. Laceaque sitiorem ut restibusaes es tumquam core posae volor remped modis volor. Doloreiur qui commolu ptatemp dolupta orem retibusam emnis et consent accullignis lomnus.</p>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <img class="img-responsive" src="http://placehold.it/600x250">
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /#tab2 -->
            <div class="tab-pane fade" id="tab3">
                <div class="row">
                    <div class="col-md-4 col-md-push-3 col-md-offset-1 col-sm-6">
                        <img class="img-responsive" src="http://placehold.it/600x500">
                    </div>
                    <!-- /.col -->
                    <div class="col-md-3 col-md-pull-4 col-sm-6">
                        <h3>Bootstrap</h3>
                        <p>Magnis modipsae lib voloratati andigen daepedor quiate aut labor. Laceaque quiae sitiorem resti est lore tumquam core posae volor uso remped modis volor. Doloreiur qui commolu ptatemp dolupta orem retibusam emnis et consent it accullignis orum lomnus.</p>
                    </div>
                    <!-- /.col -->
                    <div class="col-md-3 col-sm-6">
                        <h3>Responsive</h3>
                        <p>Magnis modipsae lib voloratati andigen daepedor quiate aut labor. Laceaque quiae sitiorem resti est lore tumquam core posae volor uso remped modis volor. Doloreiur qui commolu ptatemp dolupta orem retibusam emnis et consent it accullignis orum lomnus.</p>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /#tab3 -->
            <div class="tab-pane fade" id="tab4">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
                        <h3>WordPress</h3>
                        <p>Magnis modipsae que lib voloratati andigen daepeditem quiate ut reporemni aut labor. Laceaque quiae sitiorem rest non restibusaes es tumquam core posae volor remped modis volor. Doloreiur qui commolu ptatemp dolupta oreprerum tibusam emnis et consent accullignis.</p>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /#tab4 -->
        </div>
        <!-- /.tab-content -->
    </div>
    <!-- /.container -->
</section>