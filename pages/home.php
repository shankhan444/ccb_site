<div class="container">
		<!-- Carousel Start -->
		<div id="myCarousel" class="carousel slide" data-ride="carousel" style="margin-bottom: 20px;">
    <!-- Carousel indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>   
    <!-- Wrapper for carousel items -->
    <div class="carousel-inner">
        <div class="item active">
            <img src="<?php echo $base_url ?>images/index/slider/education.jpg" alt="First Slide">
        </div>
        <div class="item">
            <img src="<?php echo $base_url ?>images/index/slider/health.jpg" alt="Second Slide">
        </div>
        <div class="item">
            <img src="<?php echo $base_url ?>images/index/slider/garden.jpg" alt="Third Slide">
        </div>
    </div>
    <!-- Carousel controls -->
    <a class="carousel-control left" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="carousel-control right" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
</div>
		<!-- Carousel End -->

        <!-- Call to Action Well -->
        <div class="well">
                <p style="font-size: 16px; margin:auto; text-align: center">This call to action card is a great place to showcase some important information or display a clever tagline!</p>
            
        </div>

        <!-- Content Row -->
        <div class="row">
            <div class="col-lg-4 col-md-4" style="line-height: 2">
                <div class="well">
                    
                        <h2 style="margin-top: 0px;">About Us</h2>
                        <p class="card-text">Chaklala Cantonment Board (CCB) was established on 1st February, 2003 upon bifurcation of Rawalpindi Cantonment into two Cantts i.e. Rawalpindi & Chaklala Cantonments due to excess of population and for good administrative control.</p>
                    
                    <div class="card-footer">
                        <a href="#" class="btn btn-primary">More Info</a>
                    </div>
                </div>
            </div>
            <!-- /.col-md-4 -->
            <div class="col-lg-4 col-md-4">
                <div class="well">
                    
                        <h2 style="margin-top: 0px;">Our Mission</h2>
                        <p class="card-text">Chaklala Cantonment Board aims at providing high quality services with any access to all which can make a decent living place to live in and to leave a sustainable world for future generations. Our mission is to ensure pro people and efficient local governance in Cantonment and effective defense land.</p>
                    
                    <div class="card-footer">
                        <a href="#" class="btn btn-primary">More Info</a>
                    </div>
                </div>
            </div>
            <!-- /.col-md-4 -->
            
            <div class="col-lg-4 col-md-4">
                <div class="well">
                	<ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#news" aria-controls="news" role="tab" data-toggle="tab">News</a></li>
                                        <li role="presentation"><a href="#notification" aria-controls="notification" role="tab" data-toggle="tab">Notifications</a></li>
                                        
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="news">Chaklala Cantonment BoardLorem Ipsum News is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
                                        <div role="tabpanel" class="tab-pane" id="notification">Chaklala Cantonment BoardLorem Ipsum Notification is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
                                        
                                    </div>
                        
                </div>
            </div>
            <!-- /.col-md-4 -->

        </div>
        <!-- /.row -->
        </div>