<?php 

include_once 'includes/header.php';

?>

    <!-- Page Content -->
    
<?php 

    if(!empty($_GET['page'])){
        //print_r($_GET);
        $page = $_GET['page'] . ".php";
        
        //$varable_array = explode('/' ,$_SERVER['REQUEST_URI']);
        
        //$page =  $varable_array[3].".php";
        
        $pages = scandir("pages", SCANDIR_SORT_ASCENDING);
        unset($pages[0], $pages[1]);
        //print_r($pages);
        
        if(in_array($page, $pages)){
            include_once 'pages/'.$page;
        }

    }
    else {
        include_once 'pages/home.php';
    }

?>

    <!-- /.container -->
    
<?php 

include_once 'includes/footer.php';

?>