<?php $base_url = "http://".$_SERVER['HTTP_HOST']."/"; 
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Chaklala Cantonment Board - Website</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo $base_url ?>css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo $base_url ?>css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo $base_url ?>css/style.css" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $base_url ?>images/favicon-16x16.png">
    
    <script src="<?php echo $base_url ?>js/jquery-3.2.1.js" ></script>
    <script src="<?php echo $base_url ?>js/bootstrap.js"></script>
    
    <script type="text/javascript">
		$(document).ready(function(){
			
		});
			
    </script>

<style type="text/css">

</style>

</head>

<body>

<!-- Top Navbar Start -->

<nav class="navbar navbar-default">
	<div class="container">
	
	<ul class="nav navbar-nav navbar-right hidden-md hidden-lg" style="display: inline-block">
				<li class="float-left-custom"><a href="http://www.facebook.com/chaklalacb" class="fa fa-facebook fa-custom"></a></li>
				<li style='margin-right: 1em' class="float-left-custom"><a href="#" class="fa fa-twitter fa-custom"></a></li>
				<li class="float-left-custom"><a href="signup">Sign Up</a></li>
				<li class="hidden-sm hidden-xs" style='margin-top: 1em'>|</li>
				<li class="float-left-custom"><a href="signin">Sign In</a></li>
	</ul>
			
	<button type="button" class="navbar-toggle" data-toggle='collapse' data-target='#topnavbar'>
		<span class='icon-bar'></span>
		<span class='icon-bar'></span>
		<span class='icon-bar'></span>
	</button>
		<div class="collapse navbar-collapse" id="topnavbar">
		
			<!-- Menu Items on left in Navbar start -->
			
			<ul class="nav navbar-nav multi-level">
				<li class="dropdown">
					<a href="" class='dropdown-toogle' data-toggle='dropdown'>ABOUT <span class='caret'></span></a>
					<ul class='dropdown-menu'>
						<li class="dropdown-submenu hidden-sm hidden-xs">
                            <a href="#">Board</a>
                            <ul class="dropdown-menu">
                            	<li><a href="board">Board</a></li>
                                <li><a href="building_committee">Building Committee</a></li>
                            	<li><a href="assessment_committee">Assessment Committee</a></li>
                            </ul>
                        </li>
                        <li class="hidden-lg hidden-md"><a href="#">Board</a></li>
                        <li class="hidden-lg hidden-md tab-mobile-custom"><a href="board">&#8226; Board</a></li>
                        <li class="hidden-lg hidden-md tab-mobile-custom"><a href="building_committee">&#8226; Building Committee</a></li>
                        <li class="hidden-lg hidden-md tab-mobile-custom"><a href="assessment_committee">&#8226; Assessment Committee</a></li>
                        <li><a href="president">President</a></li>
                        <li><a href="ceo">CEO</a></li>
                        <li><a href="civil_members">Civil Members</a></li>
                        <li><a href="jurisdiction">Jurisdiction</a></li>
                        <li><a href="organization">Organization</a></li>
					</ul>
				</li>
				<li class="hidden-sm hidden-xs" style='margin-top: 1em'>|</li>
				<li class="dropdown">
					<a href="" class='dropdown-toogle' data-toggle='dropdown'>INFORMATION <span class='caret'></span></a>
					<ul class='dropdown-menu'>
						<li class="dropdown-submenu hidden-sm hidden-xs">
                            <a href="#">Legal</a>
                            <ul class="dropdown-menu">
                                <li><a href="laws_legal">Laws</a></li>
                            	<li><a href="policies">Policies</a></li>
                            </ul>
                          </li>
                          <li class="hidden-lg hidden-md"><a href="#">Legal</a></li>
                            <li class="hidden-lg hidden-md tab-mobile-custom"><a href="laws_legal">&#8226; Laws</a></li>
                            <li class="hidden-lg hidden-md tab-mobile-custom"><a href="policies">&#8226; Policies</a></li>
                          <li class="dropdown-submenu hidden-sm hidden-xs">
                            <a href="#">Downloads</a>
                            <ul class="dropdown-menu">
                                <li><a href="forms">Forms</a></li>
                            	<li><a href="laws_downloads">Laws</a></li>
                            	<li><a href="lists">Lists</a></li>
                            </ul>
                          </li>
                          <li class="hidden-lg hidden-md"><a href="#">Downloads</a></li>
                          <li class="hidden-lg hidden-md tab-mobile-custom"><a href="forms">&#8226; Forms</a></li>
                            <li class="hidden-lg hidden-md tab-mobile-custom"><a href="laws_downloads">&#8226; Laws</a></li>
                            <li class="hidden-lg hidden-md tab-mobile-custom"><a href="lists">&#8226; Lists</a></li>
					</ul>
				</li>
				<li class="hidden-sm hidden-xs" style='margin-top: 1em'>|</li>
				<li class="dropdown">
					<a href="" class='dropdown-toogle' data-toggle='dropdown'>LINKS <span class='caret'></span></a>
					<ul class='dropdown-menu'>
                        <li><a href="#">HQ ML&C</a></li>
					</ul>
				</li>
				<li class="hidden-sm hidden-xs" style='margin-top: 1em'>|</li>
				<li><a href="contact">CONTACT</a></li>
			</ul>
			
			<!-- Menu Items on left in Navbar start -->
			
			<!-- Menu Items on right in Navbar start -->
			
			<ul class="nav navbar-nav navbar-right hidden-xs">
				<li><a href="http://www.facebook.com/chaklalacb" class="fa fa-facebook fa-custom"></a></li>
				<li style='margin-right: 1em'><a href="#" class="fa fa-twitter fa-custom"></a></li>
				<li><a href="signup">Sign Up</a></li>
				<li class="hidden-sm hidden-xs" style='margin-top: 1em'>|</li>
				<li><a href="signin">Sign In</a></li>
			</ul>
			
			<!-- Menu Items on right in Navbar start -->
			
		</div>
		
	</div>
</nav>

<!-- Top Navbar End -->


  <div class="container">
  <!-- First Row Start -->
  <div class="row">
  <div class="col-lg-3 col-md-3">
    <div class="navbar-header">
      <a href="home"><img src="<?php echo $base_url ?>images/ccb_light.png" /></a>
    </div>
    </div>
    
    <div class="col-lg-1 col-md-1">
    </div>
    
    <!-- Second Column Start -->
    
    
    <div class="col-lg-7 col-md-7">
    	<div class="row">
    	<div class="col-lg-1 col-md-1"></div>
    	<div class="col-lg-11 col-md-11">
    	<ul class="nav navbar-nav margin-contact-custom">
    		<li>    		
    		<a class="number-custom"><i class="fa fa-phone" aria-hidden="true"></i> +92-51-9272476</a>
    		</li>
    		<li class="hidden-sm hidden-xs" style='margin-top: 1em'>|</li>
    		<li><a class="number-custom"><i class="fa fa-fax" aria-hidden="true"></i> (051) 456-7890</a></li>
    		<li class="hidden-sm hidden-xs" style='margin-top: 1em'>|</li>
    		<li><a class="number-custom"><i class="fa fa-envelope" aria-hidden="true"></i> cb.chaklala@gmail.com</a></li>
    	</ul>
    	</div>
    	</div>
    	
    	<div class="row">
    	<div class="col-lg-1 col-md-3"></div>
    	<div class="col-lg-10 col-md-9">
    	<form class="navbar-form" role="search">
        <div class="input-group" style="width: 100%">
            <input type="text" class="form-control" placeholder="Search" name="q">
            <div class="input-group-btn">
                <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
            </div>
        </div>
        </form>
    	</div>
    	
    </div>
    	
    </div>
    
    <!-- Second Column End -->
    
    </div>
    
    <!-- First row end -->
    
    <!-- Second row start -->
    
    <nav class="navbar navbar-default center-nav-custom" style="margin-top: 1em">
    <div class="container">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
    	<div class="collapse navbar-collapse padding-nav-custom" id="myNavbar">
      <ul class="nav navbar-nav" style="display: inline-block; float: none;">
        
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">CONVENIENCE <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="life_events">Life Events</a></li>
            <li><a href="rest_house">Rest House</a></li>
            <li><a href="markets">Markets</a></li>
            <li><a href="stands">Stands</a></li>
            <li><a href="parkings">Parkings</a></li>
            <li class="dropdown-submenu hidden-sm hidden-xs">
                <a href="#">Graveyards</a>
                <ul class="dropdown-menu">
                    <li><a href="coffin_carrier">Coffin Carrier</a></li>
                </ul>
            </li>
            <li class="hidden-lg hidden-md"><a href="#">Graveyards</a></li>
            <li class="hidden-lg hidden-md tab-mobile-custom"><a href="coffin_carrier">&#8226; Coffin Carrier</a></li>
            <li><a href="workshop">Workshop</a></li>
            <li><a href="festivals">Fairs & Festivals</a></li>
          </ul>
        </li>
        
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">EDUCATION <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="school">Schools &amp; Colleges</a></li>
            <li><a href="library">Reading Room &amp; Library</a></li>
          </ul>
        </li>
        
        <li class="dropdown">
					<a href="#" class='dropdown-toogle' data-toggle='dropdown'>ENGINEERING <span class='caret'></span></a>
					<ul class='dropdown-menu'>
						<li class="dropdown-submenu hidden-sm hidden-xs">
                            <a href="#">Buildings</a>
                            <ul class="dropdown-menu">
                                <li><a href="cantt_property">Cantt Fund Property</a></li>
                            </ul>
                         </li>
                         <li class="hidden-lg hidden-md"><a href="#">Buildings</a></li>
            			 <li class="hidden-lg hidden-md tab-mobile-custom"><a href="cantt_fund">&#8226; Cantt Fund Property</a></li>
                         <li class="dropdown-submenu hidden-sm hidden-xs">
                            <a href="#">Roads</a>
                            <ul class="dropdown-menu">
                                <li><a href="road_cutting">Road Cutting</a></li>
                            </ul>
                          </li>
                          <li class="hidden-lg hidden-md"><a href="#">Roads</a></li>
            				<li class="hidden-lg hidden-md tab-mobile-custom"><a href="road_cutting">&#8226; Road Cutting</a></li>
                          <li class="dropdown-submenu hidden-sm hidden-xs">
                            <a href="#">Drainage</a>
                            <ul class="dropdown-menu">
                                <li><a href="drains">Drains</a></li>
                                <li><a href="manhole">Manholes</a></li>
                            </ul>
                          </li>
                          <li class="hidden-lg hidden-md"><a href="#">Drainage</a></li>
            			 <li class="hidden-lg hidden-md tab-mobile-custom"><a href="drains">&#8226; Drains</a></li>
            			 <li class="hidden-lg hidden-md tab-mobile-custom"><a href="manhole">&#8226; Manholes</a></li>
                         <!-- <li class="dropdown-submenu hidden-sm hidden-xs"> -->
                        <li><a href="water_supply">Water Supply</a></li>
                        <!--    
                              <ul class="dropdown-menu">
                                <li><a href="water_supply">Water Supply Network</a></li>
                                <li><a href="tubewell">Tube Wells</a></li>
                                <li><a href="filtration_plant">Filtration Plants</a></li>
                            </ul>
                          </li>
                          <li class="hidden-lg hidden-md"><a href="#">Water Supply</a></li>
            			 <li class="hidden-lg hidden-md tab-mobile-custom"><a href="water_supply">&#8226; Water Supply Network</a></li>
            			 <li class="hidden-lg hidden-md tab-mobile-custom"><a href="tube_wells">&#8226; Tube Wells</a></li>
            			 <li class="hidden-lg hidden-md tab-mobile-custom"><a href="drains">&#8226; Filtration Plants</a></li>
                        -->
                        <li><a href="lighting">Lighting</a></li>
                        <li><a href="misc_improvement">Misc Public Improvements</a></li>
					</ul>
				</li>
        
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">GARDEN <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="parks">Parks</a></li>
            <li><a href="arboriculture">Arboriculture</a></li>
            <li><a href="nursery">Nursery</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="">HEALTH <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="dispensary">Dispensaries</a></li>
            <li><a href="vaccination">Vaccination</a></li>
            <li><a href="fumigation">Fumigation</a></li>
            <li><a href="food_control">Food Control</a></li>
            <li><a href="latrines">Public Latrines</a></li>
            <li><a href="slaughter">Slaughter House</a></li>
          </ul>
        </li>
        
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">LAND <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="building_control">Building Control</a></li>
            <li><a href="land_survey">Land Survey</a></li>
            <li><a href="lease">Leases</a></li>
          </ul>
        </li>
        
        <!-- <li><a href="rent_controller">RENT CONTROLLER</a></li>  -->
        
        <li class="dropdown">
					<a href="" class='dropdown-toogle' data-toggle='dropdown'>REVENUE <span class='caret'></span></a>
					<ul class='dropdown-menu'>
						<li class="dropdown-submenu hidden-sm hidden-xs">
                            <a href="#">Tax</a>
                            <ul class="dropdown-menu">
                                <li><a href="property_tax">Property Tax</a></li>
                            	<li><a href="tip">Tax on Immovable Property</a></li>
                            	<li><a href="trade_profession">Trade &amp; Profession Tax</a></li>
                            </ul>
                          </li>
                          <li class="hidden-lg hidden-md"><a href="#">Tax</a></li>
            			 <li class="hidden-lg hidden-md tab-mobile-custom"><a href="property_tax">&#8226; Property Tax</a></li>
            			 <li class="hidden-lg hidden-md tab-mobile-custom"><a href="tip">&#8226; Tax on Immovable Property</a></li>
            			 <li class="hidden-lg hidden-md tab-mobile-custom"><a href="trade_profession">&#8226; Trade &amp; Profession Tax</a></li>
                        <li class="dropdown-submenu hidden-sm hidden-xs">
                            <a href="#">Fees</a>
                            <ul class="dropdown-menu">
                                <li><a href="advertisement">Advertisements</a></li>
                            	<li><a href="bts_tower">BTS Towers</a></li>
                            	<li><a href="fee_charges">All Fees and Charges</a></li>
                            </ul>
                          </li>
                          <li class="hidden-lg hidden-md"><a href="#">Fees</a></li>
            			 <li class="hidden-lg hidden-md tab-mobile-custom"><a href="advertisement">&#8226; Advertisements</a></li>
            			 <li class="hidden-lg hidden-md tab-mobile-custom"><a href="bts_tower">&#8226; BTS Towers</a></li>
            			 <li class="hidden-lg hidden-md tab-mobile-custom"><a href="fee_charges">&#8226; All Fees and Charges</a></li>
					</ul>
				</li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">SAFETY <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="fire_brigade">Fire Brigade</a></li>
            <li class="dropdown-submenu hidden-sm hidden-xs">
                <a href="#">Animal Control</a>
                <ul class="dropdown-menu">
                    <li><a href="cattle_pound">Cattle Pound</a></li>
                    <li><a href="dog_shooting">Dog Shooting</a></li>
                </ul>
            </li>
            <li class="hidden-lg hidden-md"><a href="#">Animal Control</a></li>
            			 <li class="hidden-lg hidden-md tab-mobile-custom"><a href="cattle_pound">&#8226; Cattle Pound</a></li>
            			 <li class="hidden-lg hidden-md tab-mobile-custom"><a href="dog_shooting">&#8226; Dog Shooting</a></li>
          </ul>
        </li>
        
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">SANITATION <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="garbage">Garbage Collection</a></li>
            <li><a href="sewerage">Sewerage</a></li>
          </ul>
        </li>
        
      </ul>
    </div>
    </div>
    </nav>
    
    <!-- Second row end -->
    
  </div>